package me.flyray.bsin.facade.service;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
* @author bolei
* @description 针对表【ai_tenant_wxmp_user】的数据库操作Service
* @createDate 2023-04-28 14:02:38
*/

@Path("tenantWxmpUserService")
public interface TenantWxPlatformUserService {

    /**
     *删除
     */
    @POST
    @Path("delete")
    @Produces("application/json")
    Map<String,Object> delete(Map<String,Object> requestMap);

    /**
     *分页查询
     */
    @POST
    @Path("getPageList")
    @Produces("application/json")
    Map<String,Object> getPageList(Map<String,Object> requestMap);

}
