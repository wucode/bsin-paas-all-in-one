package me.flyray.bsin.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.flyray.bsin.server.domain.DecisionRule;

/**
* @author bolei
* @description 针对表【decision_rule】的数据库操作Mapper
* @createDate 2023-08-12 08:57:02
* @Entity generator.domain.DecisionRule
*/

@Repository
@Mapper
public interface DecisionRuleMapper extends BaseMapper<DecisionRule> {

    List<DecisionRule> getDecisionRuleList(@Param("tenantId") String tenantId);

}




