package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("processInstanceTask")
public interface BsinAdminProcessInstanceTaskService {

    /**
     * 查询流程实例任务
     * @param requestMap
     * @return
     */
    @POST
    @Path("getProcessInstanceTasks")
    @Produces("application/json")
    Map<String, Object> getProcessInstanceTasks(Map<String, Object> requestMap);

    /**
     * 查询实例任务历史
     * @param requestMap
     * @return
     */
    @POST
    @Path("getHistoricInstanceTasks")
    @Produces("application/json")
    Map<String, Object> getHistoricInstanceTasks(Map<String, Object> requestMap);


    /**
     * 查询所有任务
     */
    @POST
    @Path("getTaskByUser")
    @Produces("application/json")
    Map<String, Object> getAllTask(Map<String, Object> requestMap);

    /**
     * 根据组织架构系统用户ID
     */
    @POST
    @Path("getTaskByUser")
    @Produces("application/json")
    public Map<String, Object> getTasksByUser(Map<String, Object> requestMap);

    /**
     * 根据业务系统的客户编号查询待办
     */
    @POST
    @Path("getTaskByUser")
    @Produces("application/json")
    public Map<String, Object> getTasksByCustomerNo(Map<String, Object> requestMap);

    /**
     * 根据用户名获取候选任务
     */
    @POST
    @Path("getCandidateTaskByUser")
    @Produces("application/json")
    public Map<String,Object> getCandidateTasksByUser(Map<String, Object> requestMap);

    /**
     * 领取候选任务
     */
    @POST
    @Path("claimTaskCandidate")
    @Produces("application/json")
    public Map<String,Object> claimCandidateTask(Map<String, Object> requestMap);

    /**
     * 任务归还（如果任务拾取之后不想操作或者误拾取任务也可以进行归还任务）
     */
    @POST
    @Path("unClaimCandidateTask")
    @Produces("application/json")
    public Map<String,Object> unClaimCandidateTask(Map<String, Object> requestMap);

    /**
     * 任务转办
     */
    @POST
    @Path("transfer")
    @Produces("application/json")
    public Map<String, Object> transfer(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 任务委派
     */
    @POST
    @Path("delegateTask")
    @Produces("application/json")
    public Map<String, Object> delegateTask(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 任务解决
     */
    @POST
    @Path("resolve")
    @Produces("application/json")
    public Map<String, Object> resolve(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 任务完成
     */
    @POST
    @Path("complete")
    @Produces("application/json")
    public Map<String, Object> complete(Map<String, Object> requestMap) throws ClassNotFoundException;

}
