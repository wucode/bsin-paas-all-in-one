package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("repositoryService")
public interface BsinRepositoryService {

    /**
     * 部署流程定义和表单定义
     */
    @POST
    @Path("importProcessDefinition")
    @Produces("application/json")
    Map<String,Object> importProcessDefinition(Map<String, Object> requestMap);

}
